class Articles{
    constructor(id, titre, sous_titre, desc){
        this.id = id;
        this.titre = titre;
        this.sous_titre = sous_titre
        this.desc = desc; 
    }
}

const data = [   
    new Articles( 0, "Article 1", "En savoir plus", "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua"),
    new Articles( 1, "Article 2", "En savoir plus",  "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua"),
    new Articles( 2, "Article 3", "En savoir plus", "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua"),
    new Articles( 3, "Article 4", "En savoir plus", "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua"),
    new Articles( 4, "Article 5", "En savoir plus", "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua"),
    new Articles( 5, "Article 6", "En savoir plus", "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua"),
    new Articles( 6, "Article 7", "En savoir plus", "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua"),
    new Articles( 7, "Article 8", "En savoir plus", "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua"),
    new Articles( 8, "Article 9", "En savoir plus", "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua"),
    new Articles( 9, "Article 10", "En savoir plus", "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua"),
];

exports.getArticleById = function(id){
    return data[id]
}

exports.allArticles = data;